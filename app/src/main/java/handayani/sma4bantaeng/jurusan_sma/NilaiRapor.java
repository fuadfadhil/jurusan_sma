package handayani.sma4bantaeng.jurusan_sma;

/**
 * Created by FUAD-PC on 13/04/2017.
 */

public class NilaiRapor {

    public String bahasaIndonesia;
    public String bahasaInggris;
    public String matematika;
    public String ipa;
    public String ips;
    public String agama;
    public String jurusan;


    public NilaiRapor(String bahasaIndonesia, String bahasaInggris, String matematika, String ipa, String ips, String agama, String jurusan){
        this.bahasaIndonesia = bahasaIndonesia;
        this.bahasaInggris = bahasaInggris;
        this.matematika = matematika;
        this.ipa = ipa;
        this.ips = ips;
        this.agama = agama;
        this.jurusan = jurusan;

    }

 }
