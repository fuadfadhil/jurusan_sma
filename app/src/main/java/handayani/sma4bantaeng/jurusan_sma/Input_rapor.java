package handayani.sma4bantaeng.jurusan_sma;

/**
 * Created by FUAD-PC on 22/03/2017.
 */

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Input_rapor extends Fragment{

    private ProgressBar progressBar;
    private EditText nil_bhsInd, nil_bhsIng, nil_mat, nil_ipa, nil_ips, nil_agama;
    private Button submit;
    String notes="",bhs,ing,mat,ipa,ips,agama;
    private DatabaseReference mDb;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_rapor, container, false);

        nil_bhsInd = (EditText)rootView.findViewById(R.id.nil_bhsInd);
        nil_bhsIng = (EditText)rootView.findViewById(R.id.nil_bhsIng);
        nil_mat = (EditText)rootView.findViewById(R.id.nil_Mat);
        nil_ipa = (EditText)rootView.findViewById(R.id.nil_Ipa);
        nil_ips = (EditText)rootView.findViewById(R.id.nil_Ips);
        nil_agama = (EditText)rootView.findViewById(R.id.nil_Agama);
        submit = (Button)rootView.findViewById(R.id.btn_submit);

        progressBar = (ProgressBar)rootView.findViewById(R.id.progressBar_Rapor);

        notes =  new Input_data().getNotes();

        mDb = FirebaseDatabase.getInstance().getReference();

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                bhs = nil_bhsInd.getText().toString();
                ing = nil_bhsIng.getText().toString();
                mat = nil_mat.getText().toString();
                ipa = nil_ipa.getText().toString();
                ips = nil_ips.getText().toString();
                agama = nil_agama.getText().toString();

                if (bhs.isEmpty()||ing.isEmpty()||mat.isEmpty()||ipa.isEmpty()||ips.isEmpty()||agama.isEmpty()){
                    Toast.makeText(getContext(),"Ada Nilai Yang Kosong", Toast.LENGTH_SHORT).show();
                }else {
                    progressBar.setVisibility(View.VISIBLE);
                    tambahData(notes,bhs,ing,mat,ipa,ips,agama);
                    progressBar.setProgress(25);
                }

                nil_bhsInd.setText("");
                nil_bhsIng.setText("");
                nil_mat.setText("");
                nil_ipa.setText("");
                nil_ips.setText("");
                nil_agama.setText("");
            }
        });


        return rootView;
    }

    private void tambahData(final String no_tes, final String bhs, final String ing, final String mat, final String ipa, final String ips, final String agama){
        NilaiRapor rapor = new NilaiRapor(bhs,ing,mat,ipa,ips,agama,"");

        progressBar.setProgress(50);

        mDb.child("nilaiRapor").child(no_tes).setValue(rapor, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                String hasil="";
                if (databaseError==null) {
                    hasil = "Berhasil input data";
                    progressBar.setProgress(100);
                    new Input_data().tambahData();
                    new Input_data().setNilrapor(bhs,ing,mat,ipa,ips,agama);
                    Log.e("Nilai tambahdata : ", String.valueOf(new Input_data().getStatus_input()));
                }else if (no_tes.equals(""))
                    hasil = "Gagal input";
                else
                    hasil = "Gagal, error : "+databaseError;
                Toast.makeText(getContext(),hasil, Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.INVISIBLE);
                progressBar.setProgress(0);
            }
        });
    }
}
