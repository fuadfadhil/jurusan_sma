package handayani.sma4bantaeng.jurusan_sma;

/**
 * Created by FUAD-PC on 23/04/2017.
 */

public class NilaiUn {

    public String bahasa_indonesia;
    public String bahasa_inggris;
    public String matematika;
    public String kimia;
    public String biologi;
    public String fisika;
    public String geografi;
    public String ekonomi;
    public String sosiologi;
    public String sastra_indonesia;
    public String antropologi;
    public String bahasa_asing;

    public NilaiUn(String bahasa_indonesia, String bahasa_inggris, String matematika, String kimia, String biologi, String fisika, String geografi, String ekonomi, String sosiologi, String sastra_indonesia, String antropologi, String bahasa_asing) {
        this.bahasa_indonesia = bahasa_indonesia;
        this.bahasa_inggris = bahasa_inggris;
        this.matematika = matematika;
        this.kimia = kimia;
        this.biologi = biologi;
        this.fisika = fisika;
        this.geografi = geografi;
        this.ekonomi = ekonomi;
        this.sosiologi = sosiologi;
        this.sastra_indonesia = sastra_indonesia;
        this.antropologi = antropologi;
        this.bahasa_asing = bahasa_asing;
    }
}
