package handayani.sma4bantaeng.jurusan_sma;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class Halaman_staff extends AppCompatActivity {

    private Button logout;
    private ProgressBar progressBar;
    private RecyclerView datasiswa;
    private RecyclerView.Adapter mAdapter;
    private FloatingActionButton add_data;
    private FirebaseAuth auth;
    private DatabaseReference mDb;

    List<BiodataList> listBiodata = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        auth = FirebaseAuth.getInstance();

        setContentView(R.layout.activity_halaman_staff);

        add_data = (FloatingActionButton)findViewById(R.id.add_button);
        logout = (Button)findViewById(R.id.btn_logout);
        datasiswa = (RecyclerView)findViewById(R.id.dataSiswa);
        progressBar = (ProgressBar)findViewById(R.id.progressBar3);

        try {
            mAdapter = new BiodataAdapter(listBiodata);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            datasiswa.setLayoutManager(mLayoutManager);
            datasiswa.setItemAnimator(new DefaultItemAnimator());
            datasiswa.setAdapter(mAdapter);
        }catch (Exception error){
            Log.e("Error Adapter : ",String.valueOf(error));
        }


        tampilkanData();

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                auth.signOut();
                Intent intent = new Intent(Halaman_staff.this, activity_login.class);
                startActivity(intent);
                finish();
            }
        });

        add_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog();

            }
        });

    }

    public void tampilkanData(){
        progressBar.setVisibility(View.VISIBLE);

        mDb = FirebaseDatabase.getInstance().getReference();

        mDb.child("dataSiswa").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                listBiodata.clear();

                try{
                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()){

                        BiodataList post = dataSnapshot1.getValue(BiodataList.class);
                        listBiodata.add(post);

                    }
                    progressBar.setVisibility(View.INVISIBLE);
                    mAdapter.notifyDataSetChanged();
                }catch (Exception error ){
                    Log.e("Error get data", String.valueOf(error));
                }
                //mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void alertDialog (){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Input Nomor Tes");

        final EditText nomorTes = new EditText(this);

        nomorTes.setInputType(InputType.TYPE_CLASS_TEXT);

        builder.setView(nomorTes);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Intent  intent = new Intent(Halaman_staff.this, Input_data.class);
                intent.putExtra("nomorTes",nomorTes.getText().toString());
                startActivity(intent);

                //Toast.makeText(getApplicationContext(),noTes.getNoTes(), Toast.LENGTH_SHORT).show();
                dialog.cancel();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }
}
