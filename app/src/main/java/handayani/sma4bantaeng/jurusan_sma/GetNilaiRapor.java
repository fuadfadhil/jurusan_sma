package handayani.sma4bantaeng.jurusan_sma;

/**
 * Created by FUAD-PC on 29/04/2017.
 */

public class GetNilaiRapor {
    public Long bahasaIndonesia;
    public Long bahasaInggris;
    public Long matematika;
    public Long ipa;
    public Long ips;
    public Long agama;
    public String jurusan;

     GetNilaiRapor(){

     }

    public GetNilaiRapor(Long bahasaIndonesia, Long bahasaInggris, Long matematika, Long ipa, Long ips, Long agama, String  jurusan){
        this.bahasaIndonesia = bahasaIndonesia;
        this.bahasaInggris = bahasaInggris;
        this.matematika = matematika;
        this.ipa = ipa;
        this.ips = ips;
        this.agama = agama;
        this.jurusan = jurusan;

    }

 public Long getBhs() {
 return bahasaIndonesia;
 }

 public Long getIng() {
 return bahasaInggris;
 }

 public Long getMat() {
 return matematika;
 }

 public Long getIpa() {
 return ipa;
 }

 public Long getIps() {
 return ips;
 }

 public Long getAgama() {
 return agama;
 }

 public String getJurusan(){ return jurusan; }

}
