package handayani.sma4bantaeng.jurusan_sma;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.ContactsContract;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Input_data extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    static String notes;
    static String pilJur;
    static int status_input=0;
    static double probIPA=0;
    static double probIPS=0;
    static double probBAHASA=0;
    static String jurusan;

    public int nilrapor_IND;
    public int nilrapor_ING;
    public int nilrapor_MAT;
    public int nilrapor_IPA;
    public int nilrapor_IPS;
    public int nilrapor_Agama;

    public int nilUN_IND;
    public int nilUN_ING;
    public int nilUN_MAT;
    public int nilUN_PILIHAN;

    int getStatus_input(){
        return status_input;
    }

    public void setNilrapor(String nilrapor_IND, String nilrapor_ING, String nilrapor_MAT, String nilrapor_IPA, String nilrapor_IPS, String nilrapor_Agama) {
        this.nilrapor_IND = Integer.parseInt(nilrapor_IND);
        this.nilrapor_ING = Integer.parseInt(nilrapor_ING);
        this.nilrapor_MAT = Integer.parseInt(nilrapor_MAT);
        this.nilrapor_IPA = Integer.parseInt(nilrapor_IPA);
        this.nilrapor_IPS = Integer.parseInt(nilrapor_IPS);
        this.nilrapor_Agama = Integer.parseInt(nilrapor_Agama);

        Log.i("Cek Data : ", String.valueOf(nilrapor_IND));
        Log.i("Cek Data : ", String.valueOf(this.nilrapor_IND));

        new PredictJurusan().predictRapor(nilrapor_IND,nilrapor_ING,nilrapor_MAT,nilrapor_IPA,nilrapor_IPS,nilrapor_Agama);
    }

    public void setNilUN(String pilJur, String nilUN_IND, String nilUN_ING, String nilUN_MAT, String nilUN_PILIHAN){
        this.pilJur = pilJur;
        this.nilUN_IND = Integer.parseInt(nilUN_IND);
        this.nilUN_ING = Integer.parseInt(nilUN_ING);
        this.nilUN_MAT = Integer.parseInt(nilUN_MAT);
        this.nilUN_PILIHAN = Integer.parseInt(nilUN_PILIHAN);

        Log.i("Cek Data ind: ", String.valueOf(nilUN_IND));
        Log.i("Cek Data ind: ", String.valueOf(this.nilUN_IND));

        new PredictJurusan().predictUN(nilUN_IND,nilUN_ING,nilUN_MAT,nilUN_PILIHAN);

    }


    void tambahData(){
        try {
            if (status_input >= 2) {
                Log.e("Cek Data : ", String.valueOf(nilUN_IND));

                status_input = 0;

            } else {

                status_input++;
            }
        }catch (Exception error){
            Log.e("Do Prediction",String.valueOf(error));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);

        Bundle extras = getIntent().getExtras();
        if(extras!=null) {
            notes = extras.getString("nomorTes");
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        tabLayout.getTabAt(0).setIcon(R.drawable.ic_action_account_circle);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_action_import_contacts);
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_action_library_books);

    }

    public String getNotes (){
        return notes;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_input_data, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //Placeholder Deleted

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0:
                    Input_biodata tab1 = new Input_biodata();
                    return tab1;
                case 1:
                    Input_rapor tab2 = new Input_rapor();
                    return tab2;
                case 2:
                    Input_un tab3 = new Input_un();
                    return tab3;

                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            // Show total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Biodata";
                case 1:
                    return "Rapor";
                case 2:
                    return "UN";
            }
            return null;
        }
    }
}