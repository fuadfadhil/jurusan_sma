package handayani.sma4bantaeng.jurusan_sma;

/**
 * Created by FUAD-PC on 22/03/2017.
 */

import android.content.DialogInterface;
import android.content.Intent;
import android.opengl.EGLDisplay;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.w3c.dom.Text;

public class Input_un extends Fragment{

    private ProgressBar progressBar;
    private EditText un_bhs,un_ing,un_mat,un_kimia,un_biologi,un_fisika,un_geografi,un_ekonomi,un_sosiologi,un_sastra,un_antropologi,un_asing;
    private Button btn_ipa,btn_ips,btn_bahasa,btn_submit;
    String str_bhs,str_ing,str_mat,str_kimia,str_biologi,str_fisika,str_geografi,str_ekonomi,str_sosiologi,str_sastra,str_antropologi,str_asing;
    String notes;
    String pilJur, nilPilihan;

    private DatabaseReference mDb;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_un, container, false);

        un_bhs = (EditText)rootView.findViewById(R.id.un_bhs);
        un_ing = (EditText)rootView.findViewById(R.id.un_ing);
        un_mat = (EditText)rootView.findViewById(R.id.un_mat);
        un_biologi = (EditText)rootView.findViewById(R.id.un_biologi);
        un_kimia = (EditText)rootView.findViewById(R.id.un_kimia);
        un_fisika = (EditText)rootView.findViewById(R.id.un_fisika);
        un_geografi = (EditText)rootView.findViewById(R.id.un_geografi);
        un_ekonomi = (EditText)rootView.findViewById(R.id.un_Ekonomi);
        un_sosiologi = (EditText)rootView.findViewById(R.id.un_sosiologi);
        un_sastra = (EditText)rootView.findViewById(R.id.un_sastra);
        un_antropologi = (EditText)rootView.findViewById(R.id.un_antropologi);
        un_asing = (EditText)rootView.findViewById(R.id.un_bhs_asing);

        btn_ipa = (Button)rootView.findViewById(R.id.btn_ipa);
        btn_ips = (Button)rootView.findViewById(R.id.btn_ips);
        btn_bahasa = (Button)rootView.findViewById(R.id.btn_bahasa);
        btn_submit = (Button)rootView.findViewById(R.id.submit_un);

        progressBar = (ProgressBar)rootView.findViewById(R.id.progressBar_UN);

        notes = new Input_data().getNotes();

        btn_ipa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                un_biologi.setVisibility(View.VISIBLE);
                un_fisika.setVisibility(View.VISIBLE);
                un_kimia.setVisibility(View.VISIBLE);

                un_geografi.setVisibility(View.GONE);
                un_ekonomi.setVisibility(View.GONE);
                un_sosiologi.setVisibility(View.GONE);

                un_sastra.setVisibility(View.GONE);
                un_antropologi.setVisibility(View.GONE);
                un_asing.setVisibility(View.GONE);

                pilJur = "ipa";

            }
        });

        btn_ips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                un_biologi.setVisibility(View.GONE);
                un_fisika.setVisibility(View.GONE);
                un_kimia.setVisibility(View.GONE);

                un_geografi.setVisibility(View.VISIBLE);
                un_ekonomi.setVisibility(View.VISIBLE);
                un_sosiologi.setVisibility(View.VISIBLE);

                un_sastra.setVisibility(View.GONE);
                un_antropologi.setVisibility(View.GONE);
                un_asing.setVisibility(View.GONE);

                pilJur = "ips";
            }
        });

        btn_bahasa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                un_biologi.setVisibility(View.GONE);
                un_fisika.setVisibility(View.GONE);
                un_kimia.setVisibility(View.GONE);

                un_geografi.setVisibility(View.GONE);
                un_ekonomi.setVisibility(View.GONE);
                un_sosiologi.setVisibility(View.GONE);

                un_sastra.setVisibility(View.VISIBLE);
                un_antropologi.setVisibility(View.VISIBLE);
                un_asing.setVisibility(View.VISIBLE);

                pilJur = "bahasa";
            }
        });


        mDb = FirebaseDatabase.getInstance().getReference();


        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                str_bhs = un_bhs.getText().toString();
                str_ing = un_ing.getText().toString();
                str_mat = un_mat.getText().toString();

                str_kimia = un_kimia.getText().toString();
                str_biologi = un_biologi.getText().toString();
                str_fisika = un_fisika.getText().toString();

                str_geografi = un_geografi.getText().toString();
                str_ekonomi = un_ekonomi.getText().toString();
                str_sosiologi = un_sosiologi.getText().toString();

                str_sastra = un_sastra.getText().toString();
                str_antropologi = un_antropologi.getText().toString();
                str_asing = un_asing.getText().toString();

                progressBar.setProgress(25);

                tambahData(notes,str_bhs,str_ing,str_mat,str_kimia,str_biologi,str_fisika,str_geografi,str_ekonomi,
                        str_sosiologi,str_sastra,str_antropologi,str_asing);

            }
        });
        return rootView;
    }

    public void tambahData(final String notes, final String bhs, final String ing, final String mat, String kimia, String biologi, String fisika, String geografi,
                           String ekonomi, String sosiologi, String sastra, String antropologi, String asing){
        NilaiUn nilaiUn = new NilaiUn(bhs,ing,mat,kimia,biologi,fisika,geografi,ekonomi,sosiologi,sastra,antropologi,asing);

        progressBar.setProgress(50);

        try {
            switch (pilJur){
                case "ipa" :

                    if (kimia.equals("")) {kimia = "0";}
                    else {nilPilihan = kimia;}

                    if (biologi.equals("")) {biologi = "0";}
                    else {nilPilihan = biologi;}

                    if (fisika.equals("")) {fisika = "0";}
                    else {nilPilihan = fisika;}

                    break;

                case "ips" :

                    if (geografi.equals("")) {geografi = "0";}
                    else {nilPilihan = geografi;}

                    if (ekonomi.equals("")) {ekonomi = "0";}
                    else {nilPilihan = ekonomi;}

                    if (sosiologi.equals("")) {sosiologi = "0";}
                    else {nilPilihan = sosiologi;}

                    break;

                case "bahasa" :
                    if (sastra.equals("")){sastra = "0";}
                    else {nilPilihan = sastra;}

                    if (antropologi.equals("")) {antropologi = "0";}
                    else {nilPilihan = antropologi;}

                    if (asing.equals("")) {asing="0";}
                    else {nilPilihan = asing;}

                    break;

                default:
                    Log.e("Hasil Pilihan","Tidak Ditemukan");
            }
        }catch (Exception error){
            Log.e("Error set Pilihan",String.valueOf(error));
        }

        mDb.child("nilaiUN").child(notes).setValue(nilaiUn, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                String hasil="";
                if (databaseError==null) {
                    hasil = "Berhasil input data";
                    progressBar.setProgress(100);
                    new Input_data().tambahData();
                    new Input_data().setNilUN(pilJur,bhs,ing,mat,nilPilihan);
                    Log.e("Nilai tambahdata : ", String.valueOf(new Input_data().getStatus_input()));
                }else if (notes.equals(""))
                    hasil = "Gagal input";
                else
                    hasil = "Gagal, error : "+databaseError;
                Toast.makeText(getContext(),hasil, Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.INVISIBLE);
                progressBar.setProgress(0);
            }
        });

        un_bhs.setText("");
        un_ing.setText("");
        un_mat.setText("");

        un_biologi.setText("");
        un_kimia.setText("");
        un_fisika.setText("");

        un_ekonomi.setText("");
        un_geografi.setText("");
        un_sosiologi.setText("");

        un_antropologi.setText("");
        un_sastra.setText("");
        un_asing.setText("");


    }
}
