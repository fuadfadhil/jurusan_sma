package handayani.sma4bantaeng.jurusan_sma;

/**
 * Created by FUAD-PC on 28/03/2017.
 */

public class Biodata {

    public String nama;
    public String alamat;
    public String agama;
    public String jurusan;

    public Biodata(String nama, String alamat, String agama, String jurusan){
        this.nama = nama;
        this.alamat = alamat;
        this.agama = agama;
        this.jurusan = jurusan;
    }

    public String getNama(){
        return nama;
    }

    public String getAlamat(){
        return alamat;
    }

    public String getAgama(){
        return agama;
    }
}
