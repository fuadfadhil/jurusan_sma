package handayani.sma4bantaeng.jurusan_sma;

import android.util.Log;

/**
 * Created by FUAD-PC on 30/04/2017.
 */

public class GetUn {

    public Long bahasaIndonesia;
    public Long bahasaInggris;
    public Long matematika;

    public Long biologi;
    public Long fisika;
    public Long kimia;

    public Long geografi;
    public Long ekonomi;
    public Long sosiologi;

    public Long sastra_indonesia;
    public Long antropologi;
    public Long bahasa_asing;

    public String jurusan;

    public GetUn() {

    }

    public GetUn(Long bahasaIndonesia, Long bahasaInggris, Long matematika, Long biologi, Long fisika, Long kimia, Long geografi,
                 Long ekonomi, Long sosiologi, Long sastra_indonesia, Long antropologi, Long bahasa_asing, String jurusan) {
        this.bahasaIndonesia = bahasaIndonesia;
        this.bahasaInggris = bahasaInggris;
        this.matematika = matematika;
        this.biologi = biologi;
        this.fisika = fisika;
        this.kimia = kimia;
        this.geografi = geografi;
        this.ekonomi = ekonomi;
        this.sosiologi = sosiologi;
        this.sastra_indonesia = sastra_indonesia;
        this.antropologi = antropologi;
        this.bahasa_asing = bahasa_asing;
        this.jurusan = jurusan;
    }

    public Long getBahasaIndonesia() {
        return bahasaIndonesia;
    }

    public Long getBahasaInggris() {
        return bahasaInggris;
    }

    public Long getMatematika() {
        return matematika;
    }

    public Long getBiologi() {
        return biologi;
    }

    public Long getFisika() {
        return fisika;
    }

    public Long getKimia() {
        return kimia;
    }

    public Long getGeografi() {
        return geografi;
    }

    public Long getEkonomi() {
        return ekonomi;
    }

    public Long getSosiologi() {
        return sosiologi;
    }

    public Long getSastra_indonesia() {
        return sastra_indonesia;
    }

    public Long getAntropologi() {
        return antropologi;
    }

    public Long getBahasa_asing() {
        return bahasa_asing;
    }

    public String getJurusan() {
        return jurusan;
    }
}
