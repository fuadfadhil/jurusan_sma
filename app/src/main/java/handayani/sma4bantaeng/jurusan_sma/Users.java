package handayani.sma4bantaeng.jurusan_sma;

/**
 * Created by FUAD-PC on 22/03/2017.
 */

public class Users {
    public String nama;
    public String email;
    public String status;

    // Default constructor required for calls to
    // DataSnapshot.getValue(User.class)
    public Users() {

    }

    public Users(String nama, String email, String status) {
        this.nama = nama;
        this.email = email;
        this.status = status;
    }
}
