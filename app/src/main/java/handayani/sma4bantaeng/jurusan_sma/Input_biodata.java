package handayani.sma4bantaeng.jurusan_sma;

/**
 * Created by FUAD-PC on 22/03/2017.
 */

import android.content.DialogInterface;
import android.graphics.Color;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;

public class Input_biodata extends Fragment{

    public EditText nama,alamat,agama;
    private Button submit;
    private DatabaseReference mDb;
    private String in_no_tes,in_nama,in_alamat,in_agama;
    private ProgressBar progressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_biodata, container, false);

        nama = (EditText) rootView.findViewById(R.id.nama);
        alamat = (EditText) rootView.findViewById(R.id.alamat);
        agama = (EditText) rootView.findViewById(R.id.agama);
        submit = (Button) rootView.findViewById(R.id.submit_bio);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar_Biodata);

        in_no_tes = new Input_data().getNotes();

        //Toast.makeText(getContext(),in_no_tes, Toast.LENGTH_SHORT).show();

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDb = FirebaseDatabase.getInstance().getReference();

                in_nama = nama.getText().toString();
                in_alamat = alamat.getText().toString();
                in_agama = agama.getText().toString();

                if(in_no_tes.isEmpty()||in_nama.isEmpty()||in_alamat.isEmpty()||in_agama.isEmpty()) {
                    Toast.makeText(getContext(), "Ada data yang kosong", Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.VISIBLE);
                }else {

                    //Toast.makeText(getContext(),noTes.getNoTes(), Toast.LENGTH_SHORT).show();

                    WriteBio(in_no_tes, in_nama, in_alamat, in_agama);
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.setProgress(25);

                }
            }
        });
        return rootView;
    }

    private void WriteBio(final String no_tes, final String nama, final String alamat, final String agama){

        //Toast.makeText(getContext(),no_tes, Toast.LENGTH_SHORT).show()
        progressBar.setProgress(50);
        mDb.child("dataSiswa").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.hasChild(no_tes)){
                    AlergDialog(no_tes,nama,alamat,agama);
                    //Toast.makeText(getContext(),hasil+"", Toast.LENGTH_SHORT).show();
                }else{

                    tambahData(no_tes,nama,alamat,agama);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        this.nama.setText("");
        this.alamat.setText("");
        this.agama.setText("");
    }

    private void tambahData(final String no_tes, String nama, String alamat, String agama){
        Biodata bio = new Biodata(nama,alamat,agama,"");

        progressBar.setProgress(75);

        mDb.child("dataSiswa").child(no_tes).setValue(bio, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                String hasil="";
                if (databaseError==null) {
                    hasil = "Berhasil input data";
                    progressBar.setProgress(100);
                    new Input_data().tambahData();
                    Log.e("Nilai tambahdata : ", String.valueOf(new Input_data().getStatus_input()));
                }else if (no_tes.equals(""))
                    hasil = "Gagal input";
                else
                    hasil = "Gagal, error : "+databaseError;
                Toast.makeText(getContext(),hasil, Toast.LENGTH_SHORT).show();

                progressBar.setVisibility(View.INVISIBLE);
                progressBar.setProgress(0);
            }
        });

    }

    private void AlergDialog(final String no_tes, final String nama, final String alamat, final String agama){
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        builder.setTitle("Data Sudah Ada");

        builder.setMessage("Tekan Edit jika ingin menimpa data");

        builder.setCancelable(false);

        builder.setPositiveButton("Edit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                tambahData(no_tes,nama,alamat,agama);
                dialog.cancel();
            }
        });

        builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                progressBar.setProgress(0);
                progressBar.setVisibility(View.INVISIBLE);
            }
        });

        final AlertDialog olustur = builder.create();
        olustur.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                olustur.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.BLACK);
                olustur.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.BLACK);
            }
        });
        olustur.show();
    }
}