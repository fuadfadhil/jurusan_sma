package handayani.sma4bantaeng.jurusan_sma;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

/**
 * Created by FUAD-PC on 26/04/2017.
 */

public class BiodataAdapter extends RecyclerView.Adapter<BiodataAdapter.MyViewHolder> {

    private List <BiodataList> biodataList;

    public class MyViewHolder extends RecyclerView.ViewHolder{

        public TextView title, alamat, jurusan;

        public MyViewHolder(View itemView) {
            super(itemView);

            title = (TextView) itemView.findViewById(R.id.title_list);
            alamat = (TextView)itemView.findViewById(R.id.alamat_list);
            jurusan = (TextView)itemView.findViewById(R.id.jurusan_list);

        }
    }

    public BiodataAdapter(List<BiodataList> biodataList){
        this.biodataList = biodataList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.biodata_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        BiodataList biodata = biodataList.get(position);
        holder.title.setText(biodata.getNama());
        holder.alamat.setText(biodata.getAlamat());
        holder.jurusan.setText(biodata.getJurusan());
    }

    @Override
    public int getItemCount() {
        return biodataList.size();
    }


}
