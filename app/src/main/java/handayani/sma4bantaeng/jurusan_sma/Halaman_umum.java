package handayani.sma4bantaeng.jurusan_sma;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class Halaman_umum extends AppCompatActivity {

    private RecyclerView datasiswa;
    private Button keluar;
    private FloatingActionButton add_data;
    private FirebaseAuth auth;
    private DatabaseReference mDb;
    private RecyclerView.Adapter mAdapter;

    List<BiodataList> listBiodata = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_halaman_umum);

        auth = FirebaseAuth.getInstance();

        add_data = (FloatingActionButton)findViewById(R.id.add_button);
        keluar = (Button)findViewById(R.id.btn_keluar);
        datasiswa = (RecyclerView)findViewById(R.id.data_siswa1);


        try {
            mAdapter = new BiodataAdapter(listBiodata);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            datasiswa.setLayoutManager(mLayoutManager);
            datasiswa.setItemAnimator(new DefaultItemAnimator());
            datasiswa.setAdapter(mAdapter);
        }catch (Exception error){
            Log.e("Error adapter : ", String.valueOf(error));
        }

        tampilkanData();

        keluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    public void tampilkanData(){

        mDb = FirebaseDatabase.getInstance().getReference();

        mDb.child("dataSiswa").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                listBiodata.clear();

                try{
                    for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()){
                        BiodataList post = dataSnapshot1.getValue(BiodataList.class);
                        listBiodata.add(post);
                        //textView.setText(post.getNama());
                    }
                    mAdapter.notifyDataSetChanged();
                }catch (Exception error ){
                    Log.e("Error get data", String.valueOf(error));
                }

                //mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
