package handayani.sma4bantaeng.jurusan_sma;

import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.StringTokenizer;

/**
 * Created by FUAD-PC on 27/04/2017.
 */
public class PredictJurusan {

    private DatabaseReference mDb;

    double prob_IPA=0;
    double prob_IPS=0;
    double prob_BAHASA=0;

    //DATA UNTUK RAPOR

    public int nilrapor_IND;
    public int nilrapor_ING;
    public int nilrapor_MAT;
    public int nilrapor_IPA;
    public int nilrapor_IPS;
    public int nilrapor_Agama;

    public int [] rapor_bhsIND = {0,0,0};

    public int [] rapor_bhsING = {0,0,0};

    public int [] rapor_MAT = {0,0,0};

    public int [] rapor_IPA = {0,0,0};

    public int [] rapor_IPS = {0,0,0};

    public int [] rapor_Agama= {0,0,0};

    //DATA UNTUK UN

    public int nilUN_IND;
    public int nilUN_ING;
    public int nilUN_MAT;
    public int nilUN_PILIHAN;

    public int [] un_bhsIND = {0,0,0};

    public int [] un_bhsING = {0,0,0};

    public int [] un_MAT = {0,0,0};

    public int [] un_PILIHAN = {0,0,0};

    void predictUN(String nilUN_IND, String nilUN_ING, String nilUN_MAT, String nilUN_PILIHAN){
        this.nilUN_IND = Integer.parseInt(nilUN_IND);
        this.nilUN_ING = Integer.parseInt(nilUN_ING);
        this.nilUN_MAT = Integer.parseInt(nilUN_MAT);
        this.nilUN_PILIHAN = Integer.parseInt(nilUN_PILIHAN);

        PredictUN();

        this.prob_IPA = new Input_data().probIPA;
        this.prob_IPS = new Input_data().probIPS;
        this.prob_BAHASA = new Input_data().probBAHASA;
    }

    void predictRapor(String nilrapor_IND, String nilrapor_ING, String nilrapor_MAT, String nilrapor_IPA, String nilrapor_IPS, String nilrapor_Agama){
        this.nilrapor_IND = Integer.parseInt(nilrapor_IND);
        this.nilrapor_ING = Integer.parseInt(nilrapor_ING);
        this.nilrapor_MAT = Integer.parseInt(nilrapor_MAT);
        this.nilrapor_IPA = Integer.parseInt(nilrapor_IPA);
        this.nilrapor_IPS = Integer.parseInt(nilrapor_IPS);
        this.nilrapor_Agama = Integer.parseInt(nilrapor_Agama);

        PredictRapor();

        this.prob_IPA = new Input_data().probIPA;
        this.prob_IPS = new Input_data().probIPS;
        this.prob_BAHASA = new Input_data().probBAHASA;

    }

    void PredictRapor(){
        mDb = FirebaseDatabase.getInstance().getReference("dataTraining");

        mDb.child("dataRapor").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                int jumIND = 0;
                int jumING = 0;
                int jumMAT = 0;
                int jumIPA = 0;
                int jumIPS = 0;
                int jumAGAMA = 0;

                try{

                    for (DataSnapshot dataRapor : dataSnapshot.getChildren()){
                        GetNilaiRapor nilaiRapor = dataRapor.getValue(GetNilaiRapor.class);

                        try {
                            if (nilrapor_IND >= nilaiRapor.getBhs()) {
                                switch (nilaiRapor.getJurusan()) {
                                    case "ipa":
                                        rapor_bhsIND[0]++;
                                        break;
                                    case "ips":
                                        rapor_bhsIND[1]++;
                                        break;
                                    default:
                                        rapor_bhsIND[2]++;
                                        break;
                                }
                                jumIND++;
                            }

                            if (nilrapor_ING >= nilaiRapor.getIng()) {
                                switch (nilaiRapor.getJurusan()) {
                                    case "ipa":
                                        rapor_bhsING[0]++;
                                        break;
                                    case "ips":
                                        rapor_bhsING[1]++;
                                        break;
                                    default:
                                        rapor_bhsING[2]++;
                                        break;
                                }
                                jumING++;
                            }

                            if (nilrapor_MAT >= nilaiRapor.getMat()) {
                                switch (nilaiRapor.getJurusan()) {
                                    case "ipa":
                                        rapor_MAT[0]++;
                                        break;
                                    case "ips":
                                        rapor_MAT[1]++;
                                        break;
                                    default:
                                        rapor_MAT[2]++;
                                        break;
                                }
                                jumMAT++;
                            }

                            if (nilrapor_IPA >= nilaiRapor.getIpa()) {
                                switch (nilaiRapor.getJurusan()) {
                                    case "ipa":
                                        rapor_IPA[0]++;
                                        break;
                                    case "ips":
                                        rapor_IPA[1]++;
                                        break;
                                    default:
                                        rapor_IPA[2]++;
                                        break;
                                }
                                jumIPA++;
                            }

                            if (nilrapor_IPS >= nilaiRapor.getIps()) {
                                switch (nilaiRapor.getJurusan()) {
                                    case "ipa":
                                        rapor_IPS[0]++;
                                        break;
                                    case "ips":
                                        rapor_IPS[1]++;
                                        break;
                                    default:
                                        rapor_IPS[2]++;
                                        break;
                                }
                                jumIPS++;
                            }

                            if (nilrapor_Agama >= nilaiRapor.getAgama()) {
                                switch (nilaiRapor.getJurusan()) {
                                    case "ipa":
                                        rapor_Agama[0]++;
                                        break;
                                    case "ips":
                                        rapor_Agama[1]++;
                                        break;
                                    default:
                                        rapor_Agama[2]++;
                                        break;
                                }
                                jumAGAMA++;
                            }
                            Log.i("Data Test : ",String.valueOf(jumIND + " " + rapor_bhsIND[0] + " " + jumING + " " +rapor_bhsING[0]
                                    + " " + jumMAT + " " +rapor_MAT[0] + " " + jumIPA + " " +rapor_IPA[0] + " " + jumIPS + " " +rapor_IPS[0]
                                    + " " + jumAGAMA + " " +rapor_Agama[0]));

                        }catch (Exception error){
                            Log.e("Error Get data : ",String.valueOf(error));
                        }
                    }

                    try {
                        if(jumIND == 0)
                            jumIND = 1;
                        if (jumING == 0)
                            jumING = 1;
                        if (jumMAT == 0)
                            jumMAT = 1;
                        if (jumIPA == 0)
                            jumIPA = 1;
                        if (jumIPS == 0)
                            jumIPS = 1;
                        if (jumAGAMA == 0)
                            jumAGAMA = 1;


                        prob_IPA += ((double)rapor_bhsIND[0] / (double) jumIND) + ((double) rapor_bhsING[0] / (double) jumING) +
                                ((double) rapor_MAT[0] / (double)jumMAT) + ((double)rapor_IPA[0] / (double) jumIPA) +
                                ((double)rapor_IPS[0] / (double)jumIPS) + ((double)rapor_Agama[0] / (double)jumAGAMA);

                        prob_IPS += ((double)rapor_bhsIND[1] / (double) jumIND) + ((double) rapor_bhsING[1] / (double) jumING) +
                                ((double) rapor_MAT[1] / (double)jumMAT) + ((double)rapor_IPA[1] / (double) jumIPA) +
                                ((double)rapor_IPS[1] / (double)jumIPS) + ((double)rapor_Agama[1] / (double)jumAGAMA);

                        prob_BAHASA += ((double)rapor_bhsIND[2] / (double) jumIND) + ((double) rapor_bhsING[2] / (double) jumING) +
                                ((double) rapor_MAT[2] / (double)jumMAT) + ((double)rapor_IPA[2] / (double) jumIPA) +
                                ((double)rapor_IPS[2] / (double)jumIPS) + ((double)rapor_Agama[2] / (double)jumAGAMA);

                        Log.i("Probability IPA :",String.valueOf(prob_IPA));
                        Log.i("Probability IPS :",String.valueOf(prob_IPS));
                        Log.i("Probability BAHASA :",String.valueOf(prob_BAHASA));

                        new Input_data().probIPA = prob_IPA;
                        new Input_data().probIPS = prob_IPS;
                        new Input_data().probBAHASA = prob_BAHASA;

                        Log.v("Hasil Peluang : ", String.valueOf(new Input_data().probIPA + " " + new Input_data().probIPS + " " + new Input_data().probBAHASA));

                    }catch (Exception error){
                        Log.e("Error Prediction : ", String.valueOf(error));
                    }

                }catch (Exception error){
                    Log.e("Error in Predict : ",String.valueOf(error));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    void PredictUN(){
        mDb = FirebaseDatabase.getInstance().getReference("dataTraining");

        mDb.child("dataUN").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                int jumIND = 0;
                int jumING = 0;
                int jumMAT = 0;
                int jumPILIHAN = 0;

                int bhs_IND = 0;
                int bhs_ING = 0;
                int mat = 0;
                int nilai1 = 0;
                int nilai2 = 0;
                int nilai3 = 0;

                String jurusan = "bahasa";

                try{

                    for (DataSnapshot dataUN : dataSnapshot.getChildren()){
                        GetUn nilaiun = dataUN.getValue(GetUn.class);

                        bhs_IND = nilaiun.getBahasaIndonesia().intValue();
                        bhs_ING = nilaiun.getBahasaInggris().intValue();
                        mat = nilaiun.getMatematika().intValue();
                        jurusan = nilaiun.getJurusan();

                        switch (new Input_data().pilJur){
                            case "ipa" :
                                nilai1 = nilaiun.getBiologi().intValue();
                                nilai2 = nilaiun.getFisika().intValue();
                                nilai3 = nilaiun.getKimia().intValue();
                                break;
                            case "ips" :
                                nilai1 = nilaiun.getGeografi().intValue();
                                nilai2 = nilaiun.getEkonomi().intValue();
                                nilai3 = nilaiun.getSosiologi().intValue();
                                break;
                            case "bahasa" :
                                nilai1 = nilaiun.getSastra_indonesia().intValue();
                                nilai2 = nilaiun.getAntropologi().intValue();
                                nilai3 = nilaiun.getBahasa_asing().intValue();
                                break;
                        }

                        try {
                            Log.e("Cecking value : ", String.valueOf(jurusan));

                            if (nilUN_IND >= bhs_IND) {
                                switch (jurusan) {
                                    case "ipa":
                                        un_bhsIND[0]++;
                                        break;
                                    case "ips":
                                        un_bhsIND[1]++;
                                        break;
                                    default:
                                        un_bhsIND[2]++;
                                        break;
                                }
                                jumIND++;
                            }

                            if (nilUN_ING >= bhs_ING) {
                                switch (jurusan) {
                                    case "ipa":
                                        un_bhsING[0]++;
                                        break;
                                    case "ips":
                                        un_bhsING[1]++;
                                        break;
                                    default:
                                        un_bhsING[2]++;
                                        break;
                                }
                                jumING++;
                            }

                            if (nilUN_MAT >= mat) {
                                switch (jurusan) {
                                    case "ipa":
                                        un_MAT[0]++;
                                        break;
                                    case "ips":
                                        un_MAT[1]++;
                                        break;
                                    default:
                                        un_MAT[2]++;
                                        break;
                                }
                                jumMAT++;
                            }

                            if (nilai1!=0){
                                if (nilUN_PILIHAN >= nilai1){
                                    switch (jurusan) {
                                        case "ipa":
                                            un_PILIHAN[0]++;
                                            break;
                                        case "ips":
                                            un_PILIHAN[1]++;
                                            break;
                                        default:
                                            un_PILIHAN[2]++;
                                            break;
                                    }
                                    jumPILIHAN++;
                                }
                            }else if (nilai2!=0){
                                if (nilUN_PILIHAN >= nilai2){
                                    switch (jurusan) {
                                        case "ipa":
                                            un_PILIHAN[0]++;
                                            break;
                                        case "ips":
                                            un_PILIHAN[1]++;
                                            break;
                                        default:
                                            un_PILIHAN[2]++;
                                            break;
                                    }
                                    jumPILIHAN++;
                                }
                            }else{
                                if (nilUN_PILIHAN >= nilai3){
                                    switch (jurusan) {
                                        case "ipa":
                                            un_PILIHAN[0]++;
                                            break;
                                        case "ips":
                                            un_PILIHAN[1]++;
                                            break;
                                        default:
                                            un_PILIHAN[2]++;
                                            break;
                                    }
                                    jumPILIHAN++;
                                }
                            }

                            Log.i("Data Test : ",String.valueOf(jumIND + " " + un_bhsIND[0] + " " + jumING + " " +un_bhsING[0]
                                    + " " + jumMAT + " " +un_MAT[0] + " " + jumPILIHAN + " " + un_PILIHAN));

                        }catch (Exception error){
                            Log.e("Error Get data : ",String.valueOf(error));
                        }
                    }

                    try {
                        if(jumIND == 0)
                            jumIND = 1;
                        if (jumING == 0)
                            jumING = 1;
                        if (jumMAT == 0)
                            jumMAT = 1;
                        if (jumPILIHAN == 0)
                            jumPILIHAN = 1;

                        int status_predict=0;

                        if(prob_IPA == 0 && prob_IPA == 0 && prob_BAHASA == 0 )
                            status_predict = 0;
                        else
                            status_predict = 1;

                        prob_IPA += ((double)un_bhsIND[0] / (double) jumIND) + ((double) un_bhsING[0] / (double) jumING) +
                                ((double) un_MAT[0] / (double)jumMAT) + ((double) un_PILIHAN[0] / (double) jumPILIHAN);

                        prob_IPS += ((double)un_bhsIND[1] / (double) jumIND) + ((double) un_bhsING[1] / (double) jumING) +
                                ((double) un_MAT[1] / (double)jumMAT) + ((double) un_PILIHAN[1] / (double) jumPILIHAN);

                        prob_BAHASA += ((double)un_bhsIND[2] / (double) jumIND) + ((double) un_bhsING[2] / (double) jumING) +
                                ((double) un_MAT[2] / (double)jumMAT) + ((double) un_PILIHAN[2] / (double) jumPILIHAN);

                        Log.i("Probability IPA :",String.valueOf(prob_IPA));
                        Log.i("Probability IPS :",String.valueOf(prob_IPS));
                        Log.i("Probability BAHASA :",String.valueOf(prob_BAHASA));

                            new Input_data().probIPA = prob_IPA;
                            new Input_data().probIPS = prob_IPS;
                            new Input_data().probBAHASA = prob_BAHASA;

                        if (status_predict == 1){

                            Predict(prob_IPA,prob_IPS,prob_BAHASA);


                        }

                        Log.v("Hasil Peluang : ", String.valueOf(new Input_data().probIPA + " " + new Input_data().probIPS + " " + new Input_data().probBAHASA));

                    }catch (Exception error){
                        Log.e("Error Prediction : ", String.valueOf(error));
                    }

                }catch (Exception error){
                    Log.e("Error in Predict : ",String.valueOf(error));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    void Predict(double probIPA, double probIPS, double probBAHASA){
        String jurusan;
        double max;
        max = Math.max(probIPA,probBAHASA);

        if (max == probIPA)
            max = Math.max(probIPA,probIPS);
        else
            max = Math.max(probBAHASA,probIPS);

        if (max == probIPA)
            jurusan = "ipa";
        else if (max == probIPS)
            jurusan = "ips";
        else
            jurusan = "bahasa";
        try {
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            String notes = new Input_data().getNotes();
            DatabaseReference ref = database.getReference("dataSiswa/"+notes);

            Map<String,Object> update = new HashMap<String,Object>();

            update.put("jurusan",jurusan);

            ref.updateChildren(update);

            new Input_data().jurusan = jurusan;

            Log.i("Hasil Jurusan : ",jurusan);

        }catch (Exception error) {
            Log.e("Error set Database : ", String.valueOf(error));
        }
    }
}