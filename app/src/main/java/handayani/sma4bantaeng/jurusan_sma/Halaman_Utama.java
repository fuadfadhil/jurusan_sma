package handayani.sma4bantaeng.jurusan_sma;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Halaman_Utama extends AppCompatActivity {

    private Button btnStaff,btnUmum;
    private DatabaseReference mFireDb;
    private FirebaseAuth auth;
    private ProgressBar progressBar2;
    private int Status_Akun=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_halaman__utama);

        btnStaff = (Button) findViewById(R.id.btn_staff);
        btnUmum = (Button) findViewById(R.id.btn_umum);
        progressBar2 = (ProgressBar) findViewById(R.id.progressBar2);

        auth = FirebaseAuth.getInstance();
        mFireDb = FirebaseDatabase.getInstance().getReference();
        progressBar2.setVisibility(View.GONE);
        btnStaff.setVisibility(View.VISIBLE);
        btnUmum.setVisibility(View.VISIBLE);

        btnStaff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (auth.getCurrentUser() != null) {
                    progressBar2.setVisibility(View.VISIBLE);
                    Status_Akun = 1;
                    btnStaff.setVisibility(View.INVISIBLE);
                    btnUmum.setVisibility(View.INVISIBLE);
                    //Toast.makeText(getApplicationContext(),akun, Toast.LENGTH_SHORT).show();

                    Intent i = new Intent(Halaman_Utama.this, Halaman_staff.class);
                    startActivity(i);
                    progressBar2.setVisibility(View.GONE);
                    btnStaff.setVisibility(View.VISIBLE);
                    btnUmum.setVisibility(View.VISIBLE);

                }else{

                    Intent i = new Intent(Halaman_Utama.this, activity_login.class);
                    startActivity(i);
                }

            }
        });

        btnUmum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Halaman_Utama.this,Halaman_umum.class);
                startActivity(i);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

}
