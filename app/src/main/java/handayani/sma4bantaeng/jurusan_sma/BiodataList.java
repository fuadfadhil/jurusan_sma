package handayani.sma4bantaeng.jurusan_sma;

/**
 * Created by FUAD-PC on 26/04/2017.
 */

public class BiodataList {

    public String nama;
    public String alamat;
    public String jurusan;

    public  BiodataList(){

    }

    public BiodataList(String nama, String alamat, String jurusan) {
        this.nama = nama;
        this.alamat = alamat;
        this.jurusan = jurusan;
    }

    public String getNama() {
        return nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public String getJurusan() {
        return jurusan;
    }


}
